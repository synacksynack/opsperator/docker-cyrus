SKIP_SQUASH?=1
IMAGE=opsperator/cyrus
-include Makefile.cust

.PHONY: build
build:
	@@SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in secret service config rbac statefulset deployment; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "CYRUS_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "CYRUS_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: ocpurge
ocpurge: occheck
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml \
	    | oc delete -f- || true

.PHONY: testldap
testldap:
	@@docker rm -f testldap || echo ok
	@@docker run --name testldap \
	    --tmpfs /var:rw,size=65536k \
	    --tmpfs /var/run:rw,size=65536k  \
	    --tmpfs /run/ldap:rw,size=65536k  \
	    --tmpfs /usr/local/openldap/etc/openldap:rw,size=65536k \
	    --tmpfs /var/lib/ldap:rw,size=65536k \
	    -e OPENLDAP_CYRUS_PASSWORD=secret4242424242 \
	    -e OPENLDAP_DEBUG=yesplease \
	    -e OPENLDAP_GLOBAL_ADMIN_PASSWORD=secret \
	    -e DEBUG=yesplease \
	    -e DO_CYRUS=true \
	    -e DO_EXPORTERS=true \
	    -e DO_LEMON=true \
	    -e DO_NEXTCLOUD=true \
	    -e DO_SSP=true \
	    -e DO_SERVICEDESK=true \
	    -e DO_WHITEPAGES=true \
	    -e OPENLDAP_BIND_LDAP_PORT=1389 \
	    -e OPENLDAP_BIND_LDAPS_PORT=1636 \
	    -e OPENLDAP_INIT_DEBUG_LEVEL=256 \
	    -p 1389:1389 -p 1636:1636 \
	    -d opsperator/openldap

.PHONY: test
test:
	@@mkdir -p volume/saslauthd volume/rsyslog volume/imap || echo meh
	@@chmod -R 777 volume || echo warning
	@@docker rm -f testcyrusimapd || echo ok
	@@docker rm -f testcyrussyslog || echo ok
	@@docker rm -f testcyrussaslauthd || echo ok
	@@docker rm -f testcyrusjob || echo ok
	@@docker run --name testcyrusimapd \
	    --privileged \
	    -e OPENLDAP_BIND_PW=secret4242424242 \
	    -e OPENLDAP_PROTO=ldap \
	    -e OPENLDAP_PORT=1389 \
	    -v `pwd`/volume/saslauthd:/run/saslauthd \
	    -v `pwd`/volume/rsyslog:/run/rsyslog \
	    -v `pwd`/volume/imap:/var/spool/imap \
	    --tmpfs /var/spool/cyrus:rw,size=65536k  \
	    --tmpfs /var/lib/cyrus:rw,size=65536k  \
	    --tmpfs /var/lib/imap:rw,size=65536k  \
	    -d $(IMAGE) /start-cyrus.sh
	@@docker run --name testcyrussyslog \
	    --privileged \
	    -v `pwd`/volume/rsyslog:/run/rsyslog \
	    -d $(IMAGE) /start-syslog.sh
	@@docker run --name testcyrussaslauthd \
	    --privileged \
	    -v `pwd`/volume/saslauthd:/run/saslauthd \
	    -v `pwd`/volume/rsyslog:/run/rsyslog \
	    -d $(IMAGE) /start-saslauthd.sh
	@@docker run --name testcyrusjob \
	    --privileged \
	    -e OPENLDAP_BIND_PW=secret4242424242 \
	    -e OPENLDAP_PROTO=ldap \
	    -e OPENLDAP_PORT=1389 \
	    -v `pwd`/volume/saslauthd:/run/saslauthd \
	    -v `pwd`/volume/rsyslog:/run/rsyslog \
	    -v `pwd`/volume/imap:/var/spool/imap \
	    -d $(IMAGE) /run-job.sh
