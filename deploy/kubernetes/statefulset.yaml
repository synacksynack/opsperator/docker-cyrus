---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  labels:
    name: postfix-kube
  name: postfix-kube
  namespace: ci
spec:
  podManagementPolicy: Parallel
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      name: postfix-kube
  serviceName: postfix-kube
  template:
    metadata:
      labels:
        name: postfix-kube
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: name
                operator: In
                values:
                - postfix-kube
            topologyKey: kubernetes.io/hostname
      containers:
      - args:
        - /start-postfix.sh
        env:
        - name: DEBUG
          value: yay
        - name: DKIM_HOST
          value: 127.0.0.1
        - name: DKIM_PORT
          value: "12345"
        - name: DONT_GENERATE_MAPS
          value: yay
        - name: LMTP_HOST
          value: cyrus-kube-internal.ci.svc.cluster.local
        - name: LMTP_PORT
          value: "24"
        - name: MYDOMAIN
          value: ci.apps.intra.unetresgrossebite.com
        - name: MYHOSTNAME
          value: smtp.ci.apps.intra.unetresgrossebite.com
        - name: MYNETWORKS
          value: 10.233.0.0/17
        - name: PBLS
          value: pbl.spamhaus.org bl.spamcop.net opm.blitzed.org rbl.mail-abuse.org spamsources.fabel.dk zen.spamhaus.org zombie.dnsbl.sorbs.net
        - name: SPAMD_HOST
          value: rspamd-kube.ci.svc.cluster.local
        - name: SPAMD_PORT
          value: "11332"
        - name: TZ
          value: Europe/Paris
        image: registry.gitlab.com/synacksynack/opsperator/docker-postfix:master
        imagePullPolicy: Always
        livenessProbe:
          exec:
            command:
            - sh
            - -c
            - |
                /usr/bin/check_smtp -P -F smtp.ci.apps.intra.unetresgrossebite.com -H 127.0.0.1 -p 25 -w 2s -c 5s
          failureThreshold: 10
          initialDelaySeconds: 30
          periodSeconds: 20
          timeoutSeconds: 6
        name: postfix
        ports:
        - containerPort: 25
          protocol: TCP
        - containerPort: 465
          protocol: TCP
        - containerPort: 587
          protocol: TCP
        resources:
          limits:
            cpu: 200m
            memory: 768Mi
          requests:
            cpu: 100m
            memory: 256Mi
        securityContext:
          privileged: true
        volumeMounts:
        - mountPath: /certs
          name: pkiconf
        - mountPath: /etc/ldap
          name: ldapconf
        - mountPath: /etc/postfix
          name: data
          subPath: config
        - mountPath: /etc/saslauthd.conf
          name: authconfig
          subPath: saslauthd.conf
        - mountPath: /run/rsyslog
          name: tmp
          subPath: rsyslog
        - mountPath: /run/saslauthd
          name: tmp
          subPath: saslauthd
        - mountPath: /var/spool/postfix
          name: data
          subPath: spool
        - mountPath: /var/spool/postfix/var/run/saslauthd
          name: tmp
          subPath: saslauthd
      - args:
        - /start-syslog.sh
        env:
        - name: DEBUG
          value: yay
        - name: TZ
          value: Europe/Paris
        image: registry.gitlab.com/synacksynack/opsperator/docker-postfix:master
        imagePullPolicy: Always
        livenessProbe:
          exec:
            command:
            - sh
            - -c
            - |
              test -e /run/rsyslog/dev/log || exit 1
          failureThreshold: 10
          initialDelaySeconds: 30
          periodSeconds: 20
          successThreshold: 1
          timeoutSeconds: 2
        name: syslog
        resources:
          limits:
            cpu: 100m
            memory: 512Mi
          requests:
            cpu: 25m
            memory: 256Mi
        securityContext:
          privileged: true
        volumeMounts:
        - mountPath: /run/rsyslog
          name: tmp
          subPath: rsyslog
        - mountPath: /var/log/postfix
          name: tmp
          subPath: maillogs
      - args:
        - /start-saslauthd.sh
        env:
        - name: DEBUG
          value: yay
        - name: TZ
          value: Europe/Paris
        image: registry.gitlab.com/synacksynack/opsperator/docker-postfix:master
        imagePullPolicy: Always
        livenessProbe:
          exec:
            command:
            - sh
            - -c
            - |
              test -e /run/saslauthd/mux || exit 1
          failureThreshold: 10
          initialDelaySeconds: 30
          periodSeconds: 20
          successThreshold: 1
          timeoutSeconds: 2
        name: saslauthd
        resources:
          limits:
            cpu: 100m
            memory: 512Mi
          requests:
            cpu: 25m
            memory: 256Mi
        securityContext:
          privileged: true
        volumeMounts:
        - mountPath: /certs
          name: pkiconf
        - mountPath: /etc/ldap
          name: ldapconf
        - mountPath: /etc/saslauthd.conf.d/saslauthd.conf
          name: authconfig
          subPath: saslauthd.conf
        - mountPath: /run/rsyslog
          name: tmp
          subPath: rsyslog
        - mountPath: /run/saslauthd
          name: tmp
          subPath: saslauthd
      - args:
        - /start-opendkim.sh
        env:
        - name: DEBUG
          value: yay
        - name: DKIM_HOST
          value: 127.0.0.1
        - name: DKIM_PORT
          value: "12345"
        - name: MYNETWORKS
          value: 10.233.0.0/17 127.0.0.1/32
        - name: TZ
          value: Europe/Paris
        image: registry.gitlab.com/synacksynack/opsperator/docker-postfix:master
        imagePullPolicy: Always
        livenessProbe:
          exec:
            command:
            - sh
            - -c
            - |
              test -s /run/opendkim/opendkim.pid || exit 1
          failureThreshold: 10
          initialDelaySeconds: 30
          periodSeconds: 20
          successThreshold: 1
          timeoutSeconds: 2
        name: opendkim
        resources:
          limits:
            cpu: 100m
            memory: 512Mi
          requests:
            cpu: 10m
            memory: 128Mi
        securityContext:
          privileged: true
        volumeMounts:
        - mountPath: /etc/dkim.d/keys/ci.apps.intra.unetresgrossebite.com
          name: dkimkeys
        - mountPath: /run/rsyslog
          name: tmp
          subPath: rsyslog
      - args:
        - /run-job.sh
        env:
        - name: DEBUG
          value: yay
        - name: LMTP_HOST
          value: cyrus-kube-internal.ci.svc.cluster.local
        - name: LMTP_PORT
          value: "24"
        - name: OPENLDAP_BASE
          valueFrom:
            secretKeyRef:
              key: directory-root
              name: openldap-kube
        - name: OPENLDAP_BIND_DN_PREFIX
          value: cn=postfix,ou=services
        - name: OPENLDAP_BIND_PW
          valueFrom:
            secretKeyRef:
              key: postfix-password
              name: openldap-kube
        - name: OPENLDAP_DOMAIN
          value: ci.apps.intra.unetresgrossebite.com
        - name: OPENLDAP_HOST
          value: openldap-kube.ci.svc.cluster.local
        - name: OPENLDAP_PORT
          value: "1636"
        - name: OPENLDAP_PROTO
          value: ldaps
        - name: OPENLDAP_USERS_OBJECTCLASS
          value: wsweetUser
        - name: SMTP_RELAY
          value: smtp.vms.intra.unetresgrossebite.com
        - name: TZ
          value: Europe/Paris
        image: registry.gitlab.com/synacksynack/opsperator/docker-postfix:master
        imagePullPolicy: Always
        livenessProbe:
          exec:
            command:
            - sh
            - -c
            - |
              test -s /etc/postfix/transport || exit 1
          failureThreshold: 120
          initialDelaySeconds: 30
          periodSeconds: 20
          successThreshold: 1
          timeoutSeconds: 2
        name: job
        resources:
          limits:
            cpu: 100m
            memory: 768Mi
          requests:
            cpu: 10m
            memory: 128Mi
        securityContext:
          privileged: true
        volumeMounts:
        - mountPath: /certs
          name: pkiconf
        - mountPath: /etc/ldap
          name: ldapconf
        - mountPath: /etc/postfix
          name: data
          subPath: config
      - args:
        - --postfix.logfile_path=/var/log/postfix/maillog
        - --postfix.showq_path=/var/spool/postfix/public/showq
        - --web.listen-address=:9113
        image: registry.gitlab.com/synacksynack/opsperator/docker-postfixexporter:master
        imagePullPolicy: Always
        livenessProbe:
          failureThreshold: 30
          httpGet:
            path: /
            port: 9113
            scheme: HTTP
          initialDelaySeconds: 30
          periodSeconds: 20
          successThreshold: 1
          timeoutSeconds: 3
        name: exporter
        ports:
        - containerPort: 9113
          protocol: TCP
        readinessProbe:
          failureThreshold: 3
          httpGet:
            path: /
            port: 9113
            scheme: HTTP
          initialDelaySeconds: 5
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 3
        resources:
          limits:
            cpu: 100m
            memory: 128Mi
          requests:
            cpu: 50m
            memory: 64Mi
        securityContext:
          privileged: true
        volumeMounts:
        - mountPath: /etc/postfix
          name: data
          subPath: config
        - mountPath: /var/log/postfix
          name: tmp
          subPath: maillogs
      serviceAccount: postfix-kube
      serviceAccountName: postfix-kube
      terminationGracePeriodSeconds: 60
      volumes:
      - name: authconfig
        secret:
          defaultMode: 420
          secretName: postfix-kube
      - name: dkimkeys
        secret:
          defaultMode: 420
          secretName: postfix-kube-dkim
      - configMap:
          defaultMode: 420
          name: openldap-kube-config
        name: ldapconf
      - name: pkiconf
        projected:
          defaultMode: 420
          sources:
          - secret:
              items:
              - key: ca.crt
                path: ca.crt
              name: kube-root-ca
          - secret:
              items:
              - key: tls.crt
                path: tls.crt
              name: postfix-kube-tls
          - secret:
              items:
              - key: tls.key
                path: tls.key
              name: postfix-kube-tls
      - emptyDir: {}
        name: tmp
  updateStrategy:
    type: RollingUpdate
  volumeClaimTemplates:
  - apiVersion: v1
    kind: PersistentVolumeClaim
    metadata:
      name: data
    spec:
      accessModes:
      - ReadWriteOnce
      resources:
        requests:
          storage: 2Gi
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  labels:
    backups.opsperator.io/driver: ldap
    backups.opsperator.io/interval: daily
    backups.opsperator.io/preferredtime: "15"
    backups.opsperator.io/secret: openldap-kube
    name: openldap-kube
  name: openldap-kube
  namespace: ci
spec:
  podManagementPolicy: OrderedReady
  replicas: 2
  selector:
    matchLabels:
      name: openldap-kube
  serviceName: openldap-kube
  template:
    metadata:
      labels:
        name: openldap-kube
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: name
                operator: In
                values:
                - openldap-kube
            topologyKey: kubernetes.io/hostname
      containers:
      - env:
        - name: DEBUG
          value: yay
        - name: DO_ADMIN
          value: "true"
        - name: DO_AIRSONIC
          value: "true"
        - name: DO_ALERTMANAGER
          value: "true"
        - name: DO_ARA
          value: "true"
        - name: DO_ARTIFACTORY
          value: "true"
        - name: DO_AWX
          value: "true"
        - name: DO_BLUEMIND
          value: "false"
        - name: DO_CODIMD
          value: "true"
        - name: DO_CYRUS
          value: "true"
        - name: DO_DOKUWIKI
          value: "true"
        - name: DO_DRAW
          value: "true"
        - name: DO_ERRBIT
          value: "true"
        - name: DO_ETHERPAD
          value: "true"
        - name: DO_EXPORTERS
          value: "true"
        - name: DO_FUSION
          value: "true"
        - name: DO_GERRIT
          value: "true"
        - name: DO_GITEA
          value: "true"
        - name: DO_GOGS
          value: "true"
        - name: DO_GRAFANA
          value: "true"
        - name: DO_GREENLIGHT
          value: "true"
        - name: DO_JENKINS
          value: "false"
        - name: DO_KIWIIRC
          value: "true"
        - name: DO_LEMON
          value: "true"
        - name: DO_MATOMO
          value: "true"
        - name: DO_MATTERMOST
          value: "true"
        - name: DO_MEDUSA
          value: "true"
        - name: DO_MINIO
          value: "true"
        - name: DO_NEXTCLOUD
          value: "true"
        - name: DO_NEXUS
          value: "true"
        - name: DO_NEWZNAB
          value: "true"
        - name: DO_PEERTUBE
          value: "true"
        - name: DO_PHPLDAPADMIN
          value: "true"
        - name: DO_POSTFIX
          value: "true"
        - name: DO_PROMETHEUS
          value: "true"
        - name: DO_RELEASEBELL
          value: "true"
        - name: DO_ROCKET
          value: "true"
        - name: DO_ROUNDCUBE
          value: "true"
        - name: DO_SABNZBD
          value: "true"
        - name: DO_SERVICEDESK
          value: "true"
        - name: DO_SOGO
          value: "true"
        - name: DO_SONARQUBE
          value: "true"
        - name: DO_SOPLANNING
          value: "true"
        - name: DO_SSP
          value: "true"
        - name: DO_SYSPASS
          value: "true"
        - name: DO_THANOS
          value: "true"
        - name: DO_TINYTINYRSS
          value: "true"
        - name: DO_TRANSMISSION
          value: "true"
        - name: DO_WEKAN
          value: "true"
        - name: DO_WHITEPAGES
          value: "true"
        - name: DO_WORDPRESS
          value: "true"
        - name: DO_WSWEET
          value: "true"
        - name: DO_ZPUSH
          value: "true"
        - name: LLNG_APPNAME
          value: KubeLemon
        - name: LLNG_BACKGROUND
          value: wall_dark.jpg
        - name: LLNG_LANG
          value: en
#        - name: LLNG_REDIS_SESSIONS_DATABASE
#          value: "1"
#        - name: LLNG_SENTINEL_SESSIONS_HOST
#          value: lemon-redis-kube-client
#        - name: LLNG_SENTINEL_SESSIONS_PORT
#          value: "26379"
#        - name: LLNG_SENTINEL_SESSIONS_REPLICASET
#          value: lemon-redis-kube
#        - name: LLNG_SESSIONS_BACKEND
#          value: redis
        - name: LLNG_SKIN
          value: kubelemon
        - name: OPENLDAP_ARA_PASSWORD
          valueFrom:
            secretKeyRef:
              key: ara-password
              name: openldap-kube
        - name: OPENLDAP_ARTIFACTORY_PASSWORD
          valueFrom:
            secretKeyRef:
              key: artifactory-password
              name: openldap-kube
        - name: OPENLDAP_AUTHPROXY_PASSWORD
          valueFrom:
            secretKeyRef:
              key: authproxy-password
              name: openldap-kube
        - name: OPENLDAP_AWX_PASSWORD
          valueFrom:
            secretKeyRef:
              key: awx-password
              name: openldap-kube
        - name: OPENLDAP_BIND_LDAP_PORT
          value: "1389"
        - name: OPENLDAP_BIND_LDAPS_PORT
          value: "1636"
        - name: OPENLDAP_BLUEMIND_PASSWORD
          valueFrom:
            secretKeyRef:
              key: bluemind-password
              name: openldap-kube
        - name: OPENLDAP_CODIMD_PASSWORD
          valueFrom:
            secretKeyRef:
              key: codimd-password
              name: openldap-kube
        - name: OPENLDAP_CYRUS_PASSWORD
          valueFrom:
            secretKeyRef:
              key: cyrus-password
              name: openldap-kube
        - name: OPENLDAP_DB_BACKEND
          value: mdb
        - name: OPENLDAP_DEBUG_LEVEL
          value: "256"
        - name: OPENLDAP_DOKUWIKI_PASSWORD
          valueFrom:
            secretKeyRef:
              key: dokuwiki-password
              name: openldap-kube
        - name: OPENLDAP_ERRBIT_PASSWORD
          valueFrom:
            secretKeyRef:
              key: errbit-password
              name: openldap-kube
        - name: OPENLDAP_FUSION_PASSWORD
          valueFrom:
            secretKeyRef:
              key: fusion-password
              name: openldap-kube
        - name: OPENLDAP_GERRIT_PASSWORD
          valueFrom:
            secretKeyRef:
              key: gerrit-password
              name: openldap-kube
        - name: OPENLDAP_GLOBAL_ADMIN_PASSWORD
          valueFrom:
            secretKeyRef:
              key: global-admin-password
              name: openldap-kube
        - name: OPENLDAP_GRAFANA_OAUTH2_SECRET
          valueFrom:
            secretKeyRef:
              key: grafana-oauth2-secret
              name: openldap-kube
        - name: OPENLDAP_GRAFANA_PASSWORD
          valueFrom:
            secretKeyRef:
              key: grafana-password
              name: openldap-kube
        - name: OPENLDAP_GREENLIGHT_OAUTH2_SECRET
          valueFrom:
            secretKeyRef:
              key: greenlight-oauth2-secret
              name: openldap-kube
        - name: OPENLDAP_GREENLIGHT_PASSWORD
          valueFrom:
            secretKeyRef:
              key: greenlight-password
              name: openldap-kube
        - name: OPENLDAP_HOST_ENDPOINT
          value: openldap-kube
        - name: OPENLDAP_INIT_DEBUG_LEVEL
          value: "256"
        - name: OPENLDAP_JENKINS_PASSWORD
          valueFrom:
            secretKeyRef:
              key: jenkins-password
              name: openldap-kube
        - name: OPENLDAP_LEMONLDAP_HTTPS
          value: yep
        - name: OPENLDAP_LEMONLDAP_PASSWORD
          valueFrom:
            secretKeyRef:
              key: lemonldap-password
              name: openldap-kube
        - name: OPENLDAP_LEMONLDAP_SESSIONS_PASSWORD
          valueFrom:
            secretKeyRef:
              key: lemonldap-sessions-password
              name: openldap-kube
        - name: OPENLDAP_LEMON_HTTP_PORT
          value: "8080"
        - name: OPENLDAP_MATOMO_OAUTH2_SECRET
          valueFrom:
            secretKeyRef:
              key: matomo-oauth2-secret
              name: openldap-kube
        - name: OPENLDAP_MATOMO_PASSWORD
          valueFrom:
            secretKeyRef:
              key: matomo-password
              name: openldap-kube
        - name: OPENLDAP_MATTERMOST_OAUTH2_SECRET
          valueFrom:
            secretKeyRef:
              key: mattermost-oauth2-secret
              name: openldap-kube
        - name: OPENLDAP_MATTERMOST_PASSWORD
          valueFrom:
            secretKeyRef:
              key: mattermost-password
              name: openldap-kube
        - name: OPENLDAP_MEDUSA_PASSWORD
          valueFrom:
            secretKeyRef:
              key: medusa-password
              name: openldap-kube
        - name: OPENLDAP_MONITOR_PASSWORD
          valueFrom:
            secretKeyRef:
              key: monitor-password
              name: openldap-kube
        - name: OPENLDAP_NEXTCLOUD_PASSWORD
          valueFrom:
            secretKeyRef:
              key: nextcloud-password
              name: openldap-kube
        - name: OPENLDAP_NEXUS_PASSWORD
          valueFrom:
            secretKeyRef:
              key: nexus-password
              name: openldap-kube
        - name: OPENLDAP_ORG_SHORT
          value: KubeCi
        - name: OPENLDAP_PEERTUBE_OAUTH2_SECRET
          valueFrom:
            secretKeyRef:
              key: peertube-oauth2-secret
              name: openldap-kube
        - name: OPENLDAP_PEERTUBE_PASSWORD
          valueFrom:
            secretKeyRef:
              key: peertube-password
              name: openldap-kube
        - name: OPENLDAP_PHPLDAPADMIN_PASSWORD
          valueFrom:
            secretKeyRef:
              key: phpldapadmin-password
              name: openldap-kube
        - name: OPENLDAP_POSTFIX_PASSWORD
          valueFrom:
            secretKeyRef:
              key: postfix-password
              name: openldap-kube
        - name: OPENLDAP_RELEASEBELL_PASSWORD
          valueFrom:
            secretKeyRef:
              key: releasebell-password
              name: openldap-kube
        - name: OPENLDAP_ROCKET_PASSWORD
          valueFrom:
            secretKeyRef:
              key: rocket-password
              name: openldap-kube
        - name: OPENLDAP_ROOT_DN_PREFIX
          value: cn=Directory Manager
        - name: OPENLDAP_ROOT_DN_SUFFIX
          valueFrom:
            secretKeyRef:
              key: directory-root
              name: openldap-kube
        - name: OPENLDAP_ROOT_DOMAIN
          value: ci.apps.unetresgrossebite.com
        - name: OPENLDAP_ROOT_PASSWORD
          valueFrom:
            secretKeyRef:
              key: root-password
              name: openldap-kube
        - name: OPENLDAP_SABNZBD_PASSWORD
          valueFrom:
            secretKeyRef:
              key: sabnzbd-password
              name: openldap-kube
        - name: OPENLDAP_SERVICEDESK_PASSWORD
          valueFrom:
            secretKeyRef:
              key: servicedesk-password
              name: openldap-kube
        - name: OPENLDAP_SMTP_SERVER
          value: postfix-kube.ci.svc.cluster.local
        - name: OPENLDAP_SOGO_PASSWORD
          valueFrom:
            secretKeyRef:
              key: sogo-password
              name: openldap-kube
        - name: OPENLDAP_SONARQUBE_PASSWORD
          valueFrom:
            secretKeyRef:
              key: sonarqube-password
              name: openldap-kube
        - name: OPENLDAP_SOPLANNING_PASSWORD
          valueFrom:
            secretKeyRef:
              key: soplanning-password
              name: openldap-kube
        - name: OPENLDAP_SSO_CLIENT_PASSWORD
          valueFrom:
            secretKeyRef:
              key: ssoapp-password
              name: openldap-kube
        - name: OPENLDAP_SSP_PASSWORD
          valueFrom:
            secretKeyRef:
              key: ssp-password
              name: openldap-kube
        - name: OPENLDAP_STATEFULSET_NAME
          value: openldap-kube
        - name: OPENLDAP_SYNCREPL_PASSWORD
          valueFrom:
            secretKeyRef:
              key: syncrepl-password
              name: openldap-kube
        - name: OPENLDAP_SYSPASS_PASSWORD
          valueFrom:
            secretKeyRef:
              key: syspass-password
              name: openldap-kube
        - name: OPENLDAP_TINYTINYRSS_PASSWORD
          valueFrom:
            secretKeyRef:
              key: tinytinyrss-password
              name: openldap-kube
        - name: OPENLDAP_WEKAN_OAUTH2_SECRET
          valueFrom:
            secretKeyRef:
              key: wekan-oauth2-secret
              name: openldap-kube
        - name: OPENLDAP_WEKAN_PASSWORD
          valueFrom:
            secretKeyRef:
              key: wekan-password
              name: openldap-kube
        - name: OPENLDAP_WHITEPAGES_PASSWORD
          valueFrom:
            secretKeyRef:
              key: whitepages-password
              name: openldap-kube
        - name: OPENLDAP_WORDPRESS_PASSWORD
          valueFrom:
            secretKeyRef:
              key: wordpress-password
              name: openldap-kube
        - name: OPENLDAP_WSWEET_PASSWORD
          valueFrom:
            secretKeyRef:
              key: wsweet-password
              name: openldap-kube
        - name: OPENLDAP_ZPUSH_PASSWORD
          valueFrom:
            secretKeyRef:
              key: zpush-password
              name: openldap-kube
        - name: RESET_SSL
          value: "false"
        - name: TZ
          value: Europe/Paris
        image: registry.gitlab.com/synacksynack/opsperator/docker-openldap:master
        imagePullPolicy: Always
        livenessProbe:
          failureThreshold: 3
          initialDelaySeconds: 30
          periodSeconds: 10
          successThreshold: 1
          tcpSocket:
            port: 1389
          timeoutSeconds: 1
        name: openldap
        ports:
        - containerPort: 1389
          protocol: TCP
        - containerPort: 1636
          protocol: TCP
        readinessProbe:
          exec:
            command:
            - /bin/sh
            - -i
            - -c
            - /usr/local/bin/is-ready
          failureThreshold: 3
          initialDelaySeconds: 5
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 1
        resources:
          limits:
            cpu: 100m
            memory: 512Mi
          requests:
            cpu: 100m
            memory: 512Mi
        volumeMounts:
        - mountPath: /certs
          name: certs
        - mountPath: /etc/ldap
          name: data
          subPath: config
        - mountPath: /etc/openldap
          name: data
          subPath: config
        - mountPath: /usr/local/openldap/etc/openldap
          name: data
          subPath: config
        - mountPath: /var/lib/ldap
          name: data
          subPath: db
        - mountPath: /run
          name: run
      - env:
        - name: EXPORTER_PORT
          value: "9113"
        - name: LDAP_BASE
          valueFrom:
            secretKeyRef:
              key: directory-root
              name: openldap-kube
        - name: LDAP_BIND_DN_PREFIX
          value: cn=monitor,ou=services
        - name: LDAP_BIND_PW
          valueFrom:
            secretKeyRef:
              key: monitor-password
              name: openldap-kube
        - name: LDAP_DOMAIN
          value: ci.apps.unetresgrossebite.com
        - name: LDAP_HOST
          value: 127.0.0.1
        - name: LDAP_PORT
          value: "1389"
        image: registry.gitlab.com/synacksynack/opsperator/docker-ldapexporter:master
        imagePullPolicy: Always
        livenessProbe:
          failureThreshold: 3
          initialDelaySeconds: 30
          periodSeconds: 20
          successThreshold: 1
          tcpSocket:
            port: 9113
          timeoutSeconds: 3
        name: exporter
        ports:
        - containerPort: 9113
          protocol: TCP
        readinessProbe:
          failureThreshold: 3
          initialDelaySeconds: 5
          periodSeconds: 10
          successThreshold: 1
          tcpSocket:
            port: 9113
          timeoutSeconds: 3
        resources:
          limits:
            cpu: 300m
            memory: 320Mi
          requests:
            cpu: 300m
            memory: 320Mi
      terminationGracePeriodSeconds: 600
      volumes:
      - name: certs
        secret:
          defaultMode: 420
          secretName: openldap-kube-tls
      - emptyDir: {}
        name: run
  updateStrategy:
    type: RollingUpdate
  volumeClaimTemplates:
  - apiVersion: v1
    kind: PersistentVolumeClaim
    metadata:
      name: data
    spec:
      accessModes:
      - ReadWriteOnce
      resources:
        requests:
          storage: 8Gi
