#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

CYRUS_ADMIN=${CYRUS_ADMIN:-cyrus}
CYRUS_PASSWORD=${CYRUS_PASSWORD:-secret}
CYRUS_ACL_ADMIN=${CYRUS_ACL_ADMIN:-lrswipckaxtned}
CYRUS_ACL_READ=${CYRUS_ACL_READ:-lrs}
OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=cyrus,ou=services}"
OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
OPENLDAP_USERS_OBJECTCLASS=${OPENLDAP_USERS_OBJECTCLASS:-inetOrgPerson}
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi

if ! ls /job-state >/dev/null 2>&1; then
    STATEDIR=/tmp
else
    STATEDIR=/job-state
fi


# Fetches Mailboxes List From Cyrus

echo $(cyradm -u "$CYRUS_ADMIN" -w "$CYRUS_PASSWORD" 127.0.0.1 <<EOCADM
lm user/*
EOCADM
	) >/tmp/mboxes


# Syncs Static Shared Mailboxes ACLs from LDAP to Cyrus

echo === Querying LDAP for Static Shared Mailboxes ===
echo ===

ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
	    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
	    -b "ou=groups,$OPENLDAP_BASE" \
	    -w "$OPENLDAP_BIND_PW" \
	    "(&(objectClass=groupOfNames)(mail=*)(objectClass=wsweetSharedMailbox))" cn 2>&1 \
    | awk '/^cn:/{print $2}' \
    | while read cn
    do
	lcn=`echo "$cn" | tr [A-Z] [a-z]`
	echo "$lcn" >>$STATEDIR/current
	if ! grep "user/$lcn " /tmp/mboxes >/dev/null; then
	    cyradm -u "$CYRUS_ADMIN" -w "$CYRUS_PASSWORD" 127.0.0.1 <<EOCADM
cm user/$lcn
EOCADM
	    if test $? -ne 0; then
		echo WARNING: failed creating $cn shared mailbox
		continue
	    fi
	fi
	members=""
	for memberuid in $(ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
					-D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
					-b "ou=groups,$OPENLDAP_BASE" \
					-w "$OPENLDAP_BIND_PW" \
					"(cn=$cn)" member 2>&1 \
			    | awk '/^member: uid=/{print $2;}' \
			    | sed 's|uid=\([^,]*\),.*|\1|')
	do
	    members="$members $memberuid"
	done
	echo $(cyradm -u "$CYRUS_ADMIN" -w "$CYRUS_PASSWORD" 127.0.0.1 <<EOCADM
lam user/$lcn
EOCADM
		) >/tmp/mbacl
	for member in $members
	do
	    hasAcl=`awk "/ $member /{print $3}" /tmp/mbacl 2>/dev/null`
	    shouldhave=$CYRUS_ACL_READ
	    if test "$member" = "$CYRUS_ADMIN"; then
		shouldhave=$CYRUS_ACL_ADMIN
	    fi
	    if test "$hasAcl"; then
		if echo "$hasAcl" | grep "^$shouldhave$">/dev/null; then
		    if test "$DEBUG"; then
			echo "DEBUG: $cn/$uid acl OK"
		    fi
		    continue
		fi
	    fi
	    cyradm -u "$CYRUS_ADMIN" -w "$CYRUS_PASSWORD" 127.0.0.1 <<EOCADM
sam user/$lcn $member $shouldhave
EOCADM
	    if test $? -ne 0; then
		echo WARNING: failed resetting $member privileges over $cn
	    fi
	done
	awk '{print $2}' /tmp/mbacl | while read hasUser
	    do
		isValid=false
		if test "$hasUser" = "$CYRUS_ADMIN"; then
		    isValid=true
		fi
		for member in $members
		do
		    if test "$hasUser" = "$member"; then
			isValid=true
			break
		    fi
		done
		if ! $isValid; then
		    cyradm -u "$CYRUS_ADMIN" -w "$CYRUS_PASSWORD" 127.0.0.1 <<EOCADM
dam user/$lcn $hasUser
EOCADM
		    if test $? -ne 0; then
			echo WARNING: failed removing $hasUser privileges over $cn
		    fi
		fi
	    done
	if ! grep "^$CYRUS_ADMIN[ \t]*$CYRUS_ACL_ADMIN" /tmp/mbacl; then
	    cyradm -u "$CYRUS_ADMIN" -w "$CYRUS_PASSWORD" 127.0.0.1 <<EOCADM
sam user/$lcn $CYRUS_ADMIN $CYRUS_ACL_ADMIN
EOCADM
	    if test $? -ne 0; then
		echo WARNING: failed adding admin privileges for $CYRUS_ADMIN to $cn
	    fi
	fi
	rm -f /tmp/mbacl
    done


# Syncs Dynamic Shared Mailboxes ACLs from LDAP to Cyrus

echo === Querying LDAP for Dynamic Shared Mailboxes ===
echo ===

ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
	    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
	    -b "ou=groups,$OPENLDAP_BASE" \
	    -w "$OPENLDAP_BIND_PW" \
	    "(&(objectClass=groupOfURLs)(mail=*)(objectClass=wsweetSharedMailbox))" cn 2>&1 \
    | awk '/^cn:/{print $2}' \
    | while read cn
    do
	lcn=`echo "$cn" | tr [A-Z] [a-z]`
	if ! grep "user/$cn " /tmp/mboxes >/dev/null; then
	    cyradm -u "$CYRUS_ADMIN" -w "$CYRUS_PASSWORD" 127.0.0.1 <<EOCADM
cm user/$lcn
EOCADM
	    if test $? -ne 0; then
		echo WARNING: failed creating $cn shared mailbox
		continue
	    fi
	fi
	members=""
	memberURL=$(ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
				    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
				    -b "ou=groups,$OPENLDAP_BASE" \
				    -w "$OPENLDAP_BIND_PW" \
				    "(cn=$cn)" memberURL 2>&1 \
			| awk 'BEG{take=0}{if ($1 == "memberURL:") { print $2; take = 1; } else { if ($0 == "") { take = 0; } else { if (take == 1) { print $0 } } } }' \
			| tr '\n' '|' \
			| sed -e 's,| ,,g' -e 's,|$,,')
	searchBase=$(echo "$memberURL" | sed -e 's|^ldap[s]*://[0-9\.]*/\([^?]*\)?.*|\1|')
	searchFilter=$(echo "$memberURL" | sed -e "s|.*$searchBase[^(]*\(.*\)|\1|")
	for memberuid in $(ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
					    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
					    -b "$searchBase" \
					    -w "$OPENLDAP_BIND_PW" \
					    "$searchFilter" uid 2>&1 \
				| awk 'BEG{take=0}{if ($1 == "uid:") { print $2; take = 1; } else { if ($0 == "") { take = 0; } else { if (take == 1) { print $0 } } } }' \
				| tr '\n' '|' \
				| sed -e 's,| ,,g' -e 's,|$,,')
	do
	    members="$members $memberuid"
	done
	echo $(cyradm -u "$CYRUS_ADMIN" -w "$CYRUS_PASSWORD" 127.0.0.1 <<EOCADM
lam user/$lcn
EOCADM
		) >/tmp/mbacl
	for member in $members
	do
	    hasAcl=`awk "/^$member /{print $3}" /tmp/mbacl 2>/dev/null`
	    shouldhave=$CYRUS_ACL_READ
	    if test "$member" = "$CYRUS_ADMIN"; then
		shouldhave=$CYRUS_ACL_ADMIN
	    fi
	    if test "$hasAcl"; then
		if echo "$hasAcl" | grep "^$shouldhave$">/dev/null; then
		    if test "$DEBUG"; then
			echo "DEBUG: $cn/$uid acl OK"
		    fi
		    continue
		fi
	    fi
	    cyradm -u "$CYRUS_ADMIN" -w "$CYRUS_PASSWORD" 127.0.0.1 <<EOCADM
sam user/$lcn $member $shouldhave
EOCADM
	    if test $? -ne 0; then
		echo WARNING: failed resetting $member privileges over $cn
	    fi
	done
	awk '{print $2}' /tmp/mbacl | while read hasUser
	    do
		isValid=false
		if test "$hasUser" = "$CYRUS_ADMIN"; then
		    isValid=true
		fi
		for member in $members
		do
		    if test "$hasUser" = "$member"; then
			isValid=true
			break
		    fi
		done
		if ! $isValid; then
		    cyradm -u "$CYRUS_ADMIN" -w "$CYRUS_PASSWORD" 127.0.0.1 <<EOCADM
dam user/$lcn $hasUser
EOCADM
		    if test $? -ne 0; then
			echo WARNING: failed removing $hasUser privileges over $cn
		    fi
		fi
	    done
	if ! grep "^$CYRUS_ADMIN[ \t]*$CYRUS_ACL_ADMIN" /tmp/mbacl; then
	    cyradm -u "$CYRUS_ADMIN" -w "$CYRUS_PASSWORD" 127.0.0.1 <<EOCADM
sam user/$lcn $CYRUS_ADMIN $CYRUS_ACL_ADMIN
EOCADM
	    if test $? -ne 0; then
		echo WARNING: failed adding admin privileges for $CYRUS_ADMIN to $cn
	    fi
	fi
	rm -f /tmp/mbacl
    done
rm -f /tmp/mboxes


# Purging Spurions Shared Mailboxes

if test -s "$STATEDIR/last" -a -s "$STATEDIR/current"; then
    echo === Checking for Spurions Shared Mailboxes
    echo ===
    while read mailbox
    do
	if ! grep "^$mailbox" "$STATEDIR/current" >/dev/null; then
	    cyradm -u "$CYRUS_ADMIN" -w "$CYRUS_PASSWORD" 127.0.0.1 <<EOCADM
sam user/$mailbox $CYRUS_ADMIN all
dm user/$mailbox
EOCADM
	    if test $? -ne 0; then
		echo WARNING: failed purging mailbox $mailbox
	    fi
	fi
    done <"$STATEDIR/last"
fi
if test -s "$STATEDIR/current"; then
    mv $STATEDIR/current $STATEDIR/last
fi


#TODO: Maybe grant admin roles over specific shares to given users (additional LDAP schema might be needed)

echo === Done Refreshinng Shared Mailboxes ACLs ===
echo ===
