#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

CYRUS_ADMIN=${CYRUS_ADMIN:-cyrus}
CYRUS_DEFAULT_QUOTA=1000000
CYRUS_PASSWORD=${CYRUS_PASSWORD:-secret}
OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=cyrus,ou=services}"
OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
OPENLDAP_USERS_OBJECTCLASS=${OPENLDAP_USERS_OBJECTCLASS:-inetOrgPerson}
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi

sievewrapper()
{
    local obj cmd

    obj="$1"
    cmd="$2"
    sieveshell --user "$obj" --authname "$CYRUS_ADMIN" \
	--exec "$cmd" 127.0.0.1 <<EOSC
$CYRUS_PASSWORD
EOSC
}


# Checks Users Mailboxes

echo === Querying LDAP for Users ===
echo ===

ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
	    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
	    -b "ou=users,$OPENLDAP_BASE" \
	    -w "$OPENLDAP_BIND_PW" \
	    "(mail=*)" uid 2>&1 \
    | awk '/^uid:/{print $2}' \
    | tr [A-Z] [a-z] \
    | while read uid
    do
	if ! sievewrapper "$uid" "get default /tmp/default"; then
	    if ! sievewrapper "$uid" "put /default.sieve default"; then
		echo WARNING: failed installing default sieve script for "$uid"
	    fi
	fi
    done


# Checks Shared Mailboxes

echo === Querying LDAP for Shared Mailboxes ===
echo ===

ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
	    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
	    -b "ou=groups,$OPENLDAP_BASE" \
	    -w "$OPENLDAP_BIND_PW" \
	    "(&(|(objectClass=groupOfNames)(objectClass=groupOfURLs))(mail=*)(objectClass=wsweetSharedMailbox))" cn 2>&1 \
    | awk '/^cn:/{print $2}' \
    | tr [A-Z] [a-z] \
    | while read cn
    do
	if ! sievewrapper "$cn" "get default /tmp/default"; then
	    if ! sievewrapper "$cn" "put /default.sieve default"; then
		echo WARNING: failed installing default sieve script for "$cn"
	    fi
	fi
    done


echo === Done Refreshinng Cyrus Sieve Scripts ===
echo ===
