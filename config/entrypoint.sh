#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

if test -z "$1"; then
    echo command not set
    if test "$DEBUG"; then
	sleep 86400
    fi
    exit 1
fi

. /usr/local/bin/reset-tls.sh

CYRUS_ADMIN=${CYRUS_ADMIN:-cyrus}
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
if ! test -e /etc/imapd.conf; then
    if ! ls /etc/imapd.conf.d/* >/dev/null 2>&1; then
	OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
	CYRUS_PASSWORD=${CYRUS_PASSWORD:-demo.local}
	SMTP_HOST=${SMTP_HOST:-localhost}
	SUBMISSION_PORT=${SUBMISSION_PORT:-587}
	cat <<EOF >/etc/imapd.conf
admins: $CYRUS_ADMIN
allowplaintext: yes
autocreate_quota: -1
autocreate_inbox_folders: Sent | Drafts | Trash | Junk
autocreate_post: yes
configdirectory: /var/lib/imap
conversations: 1
conversations_db: twoskip
defaultpartition: default
defaultdomain: $OPENLDAP_DOMAIN
duplicate_db_path: /run/cyrus/db/deliver.db
hashimapspool: true
#httpmodules: prometheus
idlesocket: /run/cyrus/socket/idle
lmtpsocket: /run/cyrus/socket/lmtp
#murder1_password: $CYRUS_PASSWORD
#murder2_password: $CYRUS_PASSWORD
mboxname_lockpath: /run/cyrus/lock
notifysocket: /run/cyrus/socket/notify
partition-default: /var/spool/imap
popminpoll: 1
proc_path: /run/cyrus/proc
#proxy_authname: $CYRUS_ADMIN
ptscache_db_path:  /run/cyrus/db/ptscache.db
sasl_auto_transition: no
sasl_mech_list: PLAIN LOGIN
sasl_pwcheck_method: saslauthd
smtp_auth_authname: $CYRUS_ADMIN
smtp_auth_password: $CYRUS_PASSWORD
smtp_backend: host
smtp_host: $SMTP_HOST:$SUBMISSION_PORT
sievedir: /var/lib/imap/sieve
specialusealways: 1
statuscache_db_path: /run/cyrus/db/statuscache.db
syslog_prefix: cyrus
tls_client_ca_file: /etc/pki/cyrus-imapd/cyrus-imapd-ca.pem
tls_client_ca_dir: /etc/ssl/certs
tls_session_timeout: 1440
tls_sessions_db_path: /run/cyrus/db/tls_sessions.db
tls_server_cert: /etc/pki/cyrus-imapd/cyrus-imapd.pem
tls_server_key: /etc/pki/cyrus-imapd/cyrus-imapd-key.pem
virtdomains: off
EOF
    else
	cat /etc/imapd.conf.d/*.conf >/etc/imapd.conf
    fi
fi
if ! test -e /etc/cyrus.conf; then
    if ! ls /etc/cyrus.conf.d/* >/dev/null 2>&1; then
	cat <<EOF >/etc/cyrus.conf
START {
    recover       cmd="ctl_cyrusdb -r"
    #statscleanup  cmd="promstatsd -c"
}
SERVICES {
    imap          cmd="imapd" listen="imap" prefork=5
    imaps         cmd="imapd -s" listen="imaps" prefork=1
    #http          cmd="httpd" listen="http" prefork=2
    lmtp          cmd="lmtpd -a" listen="lmtp" prefork=0
    lmtpunix      cmd="lmtpd" listen="/run/cyrus/socket/lmtp" prefork=1
    #mupdate       cmd="/usr/libexec/cyrus-imapd/mupdate -m" listen=3905 prefork=1
    pop3          cmd="pop3d" listen="pop3" prefork=3
    pop3s         cmd="pop3d -s" listen="pop3s" prefork=1
    sieve         cmd="timsieved" listen="sieve" prefork=0
}
EVENTS {
    checkpoint    cmd="ctl_cyrusdb -c" period=30
    delprune      cmd="cyr_expire -E 3" at=0400
    deleteprune   cmd="cyr_expire -E 4 -D 28" at=0430
    expungeprune  cmd="cyr_expire -E 4 -X 28" at=0445
    tlsprune      cmd="tls_prune" at=0400
}
DAEMON {
    #promstatsd    cmd="promstatsd"
}
EOF
    else
	cat /etc/cyrus.conf.d/*.conf >/etc/cyrus.conf
    fi
fi
if ! test -e /etc/saslauthd.conf; then
    if ! ls /etc/saslauthd.conf.d/* >/dev/null 2>&1; then
	OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=cyrus,ou=services}"
	OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
	OPENLDAP_HOST=${OPENLDAP_HOST:-}
	OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
	OPENLDAP_USERS_OBJECTCLASS=${OPENLDAP_USERS_OBJECTCLASS:-inetOrgPerson}
	if test -z "$OPENLDAP_BASE"; then
	    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
	fi
	if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
	    OPENLDAP_PORT=636
	elif test -z "$OPENLDAP_PORT"; then
	    OPENLDAP_PORT=389
	fi
	cat <<EOF >/etc/saslauthd.conf
auxprop_plugin: ldapdb
ldap_auth_method: bind
ldap_bind_dn: $OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE
ldap_bind_pw: $OPENLDAP_BIND_PW
ldap_default_domain: $OPENLDAP_DOMAIN
ldap_filter: (|(&(objectClass=person)(cn=%u)(cn=$CYRUS_ADMIN))(&(objectClass=$OPENLDAP_USERS_OBJECTCLASS)(|(uid=%u)(mail=%u)(mail=%u@$OPENLDAP_DOMAIN))))
ldap_search_base: $OPENLDAP_BASE
ldap_servers: $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT
ldap_scope: sub
ldap_version: 3
EOF
	if test -s /certs/ca.crt; then
	    cat <<EOF >>/etc/saslauthd.conf
ldap_tls_check_peer: yes
ldap_tls_cacert_file: /certs/ca.crt
EOF
	elif test -f /run/secrets/kubernetes.io/serviceaccount/ca.crt; then
	    cat <<EOF >>/etc/saslauthd.conf
ldap_tls_check_peer: yes
ldap_tls_cacert_file: /run/secrets/kubernetes.io/serviceaccount/ca.crt
EOF
	else
	    cat <<EOF >>/etc/saslauthd.conf
ldap_tls_check_peer: yes
ldap_tls_cacert_dir: /etc/pki/tls/certs/
EOF
	fi
	unset OPENLDAP_BIND_DN_PREFIX OPENLDAP_BIND_PW OPENLDAP_DOMAIN \
	    OPENLDAP_HOST OPENLDAP_PROTO OPENLDAP_USERS_OBJECTCLASS \
	    OPENLDAP_BASE OPENLDAP_PORT
    else
	cat /etc/saslauthd.conf.d/*.conf >/etc/saslauthd.conf
    fi
fi
ln -sf /run/rsyslog/dev/log /dev/log

exec $@
