#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

CYRUS_ADMIN=${CYRUS_ADMIN:-cyrus}
CYRUS_DEFAULT_QUOTA=1000000
CYRUS_PASSWORD=${CYRUS_PASSWORD:-secret}
OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=cyrus,ou=services}"
OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
OPENLDAP_USERS_OBJECTCLASS=${OPENLDAP_USERS_OBJECTCLASS:-inetOrgPerson}
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi

quotawrapper()
{
    local quota mailbox has
    mailbox=$1
    quota=$2
    if test -z "$quota"; then
	quota="$CYRUS_DEFAULT_QUOTA"
    fi
    # https://www.cyrusimap.org/imap/reference/manpages/systemcommands/cyradm.html#setquota
    # https://datatracker.ietf.org/doc/html/rfc2087#section-3
    # by RFC, IMAP quota unit is Kbyte - LDAP stores it in bytes
    quota=`expr $quota / 1024`
    if test "$quota" -gt 2100000000; then
	#don't ask / FIXME
	quota=2100000000
    fi
    echo $(cyradm -u "$CYRUS_ADMIN" -w "$CYRUS_PASSWORD" 127.0.0.1 <<EOCADM
lq user/$mailbox
EOCADM
	) >/tmp/usrquota
    has=`cat /tmp/usrquota | cut -d/ -f2 | awk '{print $1}'`
    rm -f /tmp/usrquota
    if test "$has"; then
	if echo "$has" | grep "^$quota$">/dev/null; then
	    if test "$DEBUG"; then
		echo "DEBUG: $mailbox quota OK"
	    fi
	    return 0
	fi
    fi
    cyradm -u "$CYRUS_ADMIN" -w "$CYRUS_PASSWORD" 127.0.0.1 <<EOCADM
sq user/$mailbox $quota
EOCADM
}


# Syncs User Mailboxes Quota

echo === Querying LDAP for User Quotas ===
echo ===

ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
	    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
	    -b "ou=users,$OPENLDAP_BASE" \
	    -w "$OPENLDAP_BIND_PW" \
	    "(mail=*)" uid 2>&1 \
    | awk '/^uid:/{print $2}' \
    | while read uid
    do
	luid=`echo "$uid" | tr [A-Z] [a-z]`
	mailQuota=$(ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
				    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
				    -b "ou=users,$OPENLDAP_BASE" \
				    -w "$OPENLDAP_BIND_PW" \
				    "(uid=$uid)" mailQuota 2>&1 \
			| awk 'BEG{take=0}{if ($1 == "mailQuota:") { print $2; take = 1; } else { if ($0 == "") { take = 0; } else { if (take == 1) { print $0 } } } }' \
			| tr '\n' '|' \
			| sed -e 's,| ,,g' -e 's,|$,,')
	if ! quotawrapper "$luid" "$mailQuota"; then
	    echo WARNING: failed setting $uid quota to $mailQuota
	fi
    done


# Syncs Shared Mailboxes Quota

echo === Querying LDAP for Shared Mailboxes Quota ===
echo ===

ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
	    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
	    -b "ou=groups,$OPENLDAP_BASE" \
	    -w "$OPENLDAP_BIND_PW" \
	    "(&(|(objectClass=groupOfNames)(objectClass=groupOfURLs))(mail=*)(objectClass=wsweetSharedMailbox))" cn 2>&1 \
    | awk '/^cn:/{print $2}' \
    | while read cn
    do
	lcn=`echo "$cn" | tr [A-Z] [a-z]`
	mailQuota=$(ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
				    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
				    -b "ou=groups,$OPENLDAP_BASE" \
				    -w "$OPENLDAP_BIND_PW" \
				    "(cn=$cn)" mailQuota 2>&1 \
			| awk 'BEG{take=0}{if ($1 == "mailQuota:") { print $2; take = 1; } else { if ($0 == "") { take = 0; } else { if (take == 1) { print $0 } } } }' \
			| tr '\n' '|' \
			| sed -e 's,| ,,g' -e 's,|$,,')
	if ! quotawrapper "$lcn" "$mailQuota"; then
	    echo WARNING: failed setting $cn quota to $mailQuota
	fi
    done

echo === Done Refreshinng Cyrus Quotas ===
echo ===
