#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

if ! test -d /var/lib/imap/db; then
    if test -d /var/lib/imap-ref; then
	cp -rvp /var/lib/imap-ref/* /var/lib/imap/
    fi
fi

while :
do
    if test -e /run/rsyslog/dev/log; then
	echo rsyslogd ready
	break
    fi
    echo waiting for rsyslogd ...
    sleep 10
done
while :
do
    if test -e /run/saslauthd/mux; then
	echo saslauthd ready
	break
    fi
    echo waiting for saslauthd ...
    sleep 10
done

mkdir -p /run/cyrus/socket
chown -R cyrus:mail /run/cyrus /var/spool/imap /var/lib/imap

ca_file=$(awk '/^tls_client_ca_file:/{ print $2 }' /etc/imapd.conf)
cert_file=$(awk '/^tls_server_cert:/{ print $2 }' /etc/imapd.conf)
cert_key_file=$(awk '/^tls_server_key:/{ print $2 }' /etc/imapd.conf)

if ! test -s "$cert_file" -a -s "$cert_key_file" -a -s "$ca_file"; then
    if test -s /certs/tls.crt -a -s /certs/tls.key -a -s /certs/ca.crt; then
	cat /certs/tls.crt >"$cert_file"
	cat /certs/tls.key >"$cert_key_file"
	cat /certs/ca.crt >"$ca_file"
    else
	/usr/bin/sscg --package cyrus-imapd \
	    --cert-file "$cert_file" \
	    --cert-key-file "$cert_key_file" \
	    --ca-file "$ca_file"
	chown cyrus "$cert_file"
	chown cyrus "$cert_key_file"
	chown cyrus "$ca_file"
    fi
fi

# NO PROMETHEUS IN 3.0
#addToConf()
#{
#    local key val file
#    if test -z "$1" -o -z "$2"; then
#	return 1
#    fi
#    key=$1
#    val=$2
#    if test -z "$3"; then
#	file=/etc/imapd.conf
#    else
#	file=$3
#    fi
#    if grep "^[ \t]*$key:" "$file" >/dev/null; then
#	sed -i "s|^[ \t]*$key:.*|$key: $val|" "$file"
#    else
#	echo "$key: $val" >>"$file"
#    fi
#}
#
#if test "$DO_PROMETHEUS"; then
#    mkdir -p /tmp/prom-stats
#    chown cyrus:mail /tmp/prom-stats
#    addToConf prometheus_enabled 1
#    addToConf prometheus_need_auth none
#    addToConf prometheus_update_freq ${PROMETHEUS_INTERVAL:-10s}
#    addToConf prometheus_stats_dir /tmp/prom-stats
#    sed -i \
#	-e 's| #statscleanup| statscleanup|' \
#	-e 's| #promstatsd| promstatsd|' \
#	-e 's| #http| http|' \
#	/etc/cyrus.conf
#else
#    addToConf prometheus_enabled 0
#    sed -i \
#	-e 's| statscleanup| #statscleanup|' \
#	-e 's| promstatsd| #promstatsd|' \
#	-e 's| http| #http|' \
#	/etc/cyrus.conf
#fi

echo Starting Cyrus
exec /usr/libexec/cyrus-imapd/cyrus-master -M /etc/cyrus.conf -C /etc/imapd.conf
