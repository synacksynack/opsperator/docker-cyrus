#!/bin/sh

find /certs /run/secrets/kubernetes.io/serviceaccount -name '*.crt' 2>/dev/null \
    | while read f
	do
	    if ! openssl x509 -text -noout -in "$f" 2>&1 \
		    | grep CA:TRUE >/dev/null; then
		continue
	    fi
	    d=`echo $f | sed 's|/|-|g'`
	    cat "$f" >"/etc/pki/ca-trust/source/anchors/kube$d"
	done

if ls /etc/pki/ca-trust/source/anchors/* >/dev/null 2>&1; then
    update-ca-trust
fi
