#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

while :
do
    if test -e /run/rsyslog/dev/log; then
	echo rsyslogd ready
	break
    fi
    echo waiting for rsyslogd ...
    sleep 10
done

exec /usr/sbin/saslauthd -O /etc/saslauthd.conf -c -V -d -m /run/saslauthd -a ldap
