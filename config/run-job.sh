#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

. /usr/local/bin/reset-tls.sh

WAIT_INTERVAL=${WAIT_INTERVAL:-3600}
if ! test "$WAIT_INTERVAL" -ge 300 >/dev/null 2>&1; then
    WAIT_INTERVAL=3600
fi

while ! ls /var/spool/imap/ | grep -E '[a-zA-Z]' >/dev/null
do
    echo "Waiting for first mailboxes to show up..."
    sleep 600
done

while :
do
    ds=`date +%s`
    echo "Starting Cyrus Quotas Refresh on $(date)"
    . /usr/local/bin/makequotas.sh
    echo "Starting Cyrus Sieve Scripts Provisioning on $(date)"
    . /usr/local/bin/makesieve.sh
    echo "Starting Cyrus Shared Mailboxes Provisioning on $(date)"
    . /usr/local/bin/makeshares.sh
    de=`date +%s`
    elapsed=`expr $ds - $ds`
    w=`expr $WAIT_INTERVAL - $elapsed`
    if test $w -lt 300; then
	w=300
    fi
    cat <<EOF
Done on `date` - ran $elapsed seconds
Waiting $w seconds until next refresh
EOF
    sleep $w
done
