#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

exec rsyslogd -n -f /etc/rsyslog.conf -i /run/rsyslogd.pid
