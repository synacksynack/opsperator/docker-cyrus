require ["fileinto","mailbox"];

if header :contains "X-Spam" "Yes" {
    fileinto :create "INBOX.Junk";
    stop;
}
if header :contains "X-Spam-Flag" "YES" {
    fileinto :create "INBOX.Junk";
    stop;
}
