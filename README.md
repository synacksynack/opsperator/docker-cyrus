# k8s Cyrus

Cyrus Base image for Kubernetes.

WARNING: privileged containers!

Forked from https://github.com/fabiomontefuscolo/docker-cyrusimapd

Depends on a Cyrus server, such as
https://gitlab.com/synacksynack/opsperator/docker-cyrus

Depends on an OpenLDAP server, such as
https://gitlab.com/synacksynack/opsperator/docker-openldap

Depends on a Postfix server, such as
https://gitlab.com/synacksynack/opsperator/docker-postfix

Build with:

```
$ make build
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Test LDAP auth:

```
$ kubectl exec -n ci deploy/cyrus-kube -- testsaslauthd -u admin0 -p test-admin-pw
$ kubectl exec -n ci -it deploy/cyrus-kube -- cyradm -u cyrus 127.0.0.1
Password: <enter password from Cyrus LDAP Account>
lm
...
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name               |    Description                     | Default                                                     |
| :----------------------------- | ---------------------------------- | ----------------------------------------------------------- |
|  `CYRUS_ADMIN`                 | Cyrus Admin Username (in LDAP)     | `cyrus` (should match cyrus use CN in LDAP)                 |
|  `CYRUS_DEFAULT_QUOTA`         | Cyrus Mailbox Quota                | `1000000` (if mailQuota not found for an LDAP user)         |
|  `CYRUS_PASSWORD`              | Cyrus Admin Password (in LDAP)     | `secret`                                                    |
|  `OPENLDAP_BASE`               | OpenLDAP Base                      | seds `OPENLDAP_DOMAIN`, default produces `dc=demo,dc=local` |
|  `OPENLDAP_BIND_DN_RREFIX`     | OpenLDAP Bind DN Prefix            | `cn=cyrus,ou=services`                                      |
|  `OPENLDAP_BIND_PW`            | OpenLDAP Bind Password             | `secret`                                                    |
|  `OPENLDAP_DOMAIN`             | OpenLDAP Domain Name               | `demo.local`                                                |
|  `OPENLDAP_HOST`               | OpenLDAP Backend Address           | undef                                                       |
|  `OPENLDAP_PORT`               | OpenLDAP Bind Port                 | `389` or `636` depending on `OPENLDAP_PROTO`                |
|  `OPENLDAP_PROTO`              | OpenLDAP Proto                     | `ldap`                                                      |
|  `OPENLDAP_USERS_OBJECTCLASS`  | OpenLDAP Users ObjectClass         | `inetOrgPerson`                                             |
|  `SMTP_HOST`                   | Cyrus SMTP Relay                   | `127.0.0.1`                                                 |
|  `SUBMISSION_PORT`             | Cyrus SMTP Submission Port         | `587`                                                       |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point | Description                                 |
| :------------------ | ------------------------------------------- |
|  `/certs`           | Cyrus Certificates & Authorities (optional) |
|  `/run/rsyslog`     | Cyrus Rsyslog Socket                        |
|  `/run/saslauthd`   | Cyrus Saslauthd Socket                      |
|  `/var/lib/imap`    | Cyrus Configuration Directories             |
|  `/var/spool/imap`  | Cyrus Mailboxes                             |
